FROM registry.gitlab.com/dreamer-labs/repoman/dl-molecule-stable/image:latest
RUN { echo -e "\n[INFO] Creating tempdir for maniac workdir content."; \
      mkdir -p /tmp/maniac/ || exit 1; };
ADD . /tmp/maniac/
RUN { echo -e "\n[INFO] Cloning git-tracked files from maniac workdir into /maniac."; \
      git clone /tmp/maniac/ /maniac || exit 1; };
WORKDIR /
CMD /bin/bash
